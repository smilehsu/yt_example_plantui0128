const COLORS = {
    white: '#fff',
    dark: '#000',
    red: '#F52A2A',
    light: '#F1F1F1',
    green: '#00B761',
    orange: '#FF8F00',
    pink: '#FFC0CB',
    grey: "#808080",
};

export default COLORS;
