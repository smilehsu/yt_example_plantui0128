import { StyleSheet, Text, View, Image } from 'react-native';
import React from 'react';
import { SafeAreaView } from 'react-native-safe-area-context';
import Icon from 'react-native-vector-icons/MaterialIcons';
import COLORS from '../../consts/Colors';

const DetailsScreen = ({ navigation, route }) => {
    const plant = route.params
    // console.log(plant)
    return (
        <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.white }}>
            <View style={styles.Header}>
                {/* 回上一頁的箭頭 圖示 */}
                <Icon name="arrow-back" size={28} onPress={() => navigation.goBack()} />
                <Icon name="shopping-cart" size={28} />
            </View>
            {/* 商品圖 */}
            <View style={styles.ImageContainer}>
                <Image
                    source={plant.img}
                    style={{ flex: 1, resizeMode: 'contain' }}
                />
            </View>
            {/* 商品簡介 */}
            <View style={styles.DetailsContainer}>
                <View style={{
                    marginLeft: 20,
                    flexDirection: "row",
                    alignItems: "flex-end"
                }}>
                    <View style={styles.Line} />
                    <Text style={{ fontSize: 18, fontWeight: "bold" }}>Best choice</Text>
                </View>
                {/* 商品名稱 */}
                <View style={{
                    marginLeft: 20,
                    marginTop: 20,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center"
                }}>
                    <Text style={{ fontSize: 22, fontWeight: "bold" }}>{plant.name}</Text>
                    {/* 價錢的標籤區塊 */}
                    <View style={styles.PriceTag}>
                        {/* 價錢 text */}
                        <Text style={{
                            marginLeft: 15,
                            fontSize: 16,
                            fontWeight: "bold",
                            color: COLORS.white
                        }}>${plant.price}
                        </Text>
                    </View>
                </View>
                {/* 商品簡介 區塊 */}
                <View style={{
                    paddingHorizontal: 20,
                    marginTop: 10
                }}>
                    <Text style={{ fontSize: 20, fontWeight: "bold" }}>About</Text>
                    {/* 商品介紹文字 */}
                    <Text style={{ color: COLORS.grey, fontSize: 16, lineHeight: 22, marginTop: 10 }}>{plant.about}</Text>
                    {/* 商品訂購 區塊 */}
                    <View style={{ marginTop: 20, flexDirection: "row", justifyContent: "space-between" }}>
                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                            <View style={styles.BorderBtn}>
                                <Text style={styles.BorderBtnText}>-</Text>
                            </View>
                            <Text style={{ marginHorizontal: 10, fontSize: 20, fontWeight: "bold" }}>1</Text>
                            <View style={styles.BorderBtn}>
                                <Text style={styles.BorderBtnText}>+</Text>
                            </View>
                        </View>
                        <View style={styles.BuyBtn}>
                            <Text style={{ color: COLORS.white, fontSize: 18, fontWeight: "bold" }}>Buy</Text>
                        </View>
                    </View>
                </View>
            </View>
        </SafeAreaView >
    );
};

export default DetailsScreen;

const styles = StyleSheet.create({
    Header: {
        paddingHorizontal: 20,
        marginTop: 20,
        flexDirection: "row",
        justifyContent: "space-between",
    },
    ImageContainer: {
        flex: 0.3,
        marginTop: 20,
        justifyContent: "center",
        alignItems: "center",

    },
    DetailsContainer: {
        flex: 0.7,
        backgroundColor: COLORS.light,
        marginHorizontal: 7,
        marginBottom: 7,
        borderRadius: 20,
        marginTop: 30,
        paddingTop: 30,
    },
    Line: {
        width: 25,
        height: 2,
        backgroundColor: COLORS.dark,
        marginBottom: 5,
        marginRight: 3,
    },
    PriceTag: {
        backgroundColor: COLORS.green,
        width: 80,
        height: 40,
        borderTopLeftRadius: 20,
        borderBottomLeftRadius: 20,
        justifyContent: "center",

    },
    BorderBtn: {
        borderColor: COLORS.grey,
        borderWidth: 1,
        borderRadius: 5,
        width: 60,
        height: 40,
        justifyContent: "center",
        alignItems: "center"
    },
    BorderBtnText: {
        fontSize: 28,
        fontWeight: "bold",
    },
    BuyBtn: {
        width: 100,
        height: 40,
        backgroundColor: COLORS.green,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 20,

    },
});
